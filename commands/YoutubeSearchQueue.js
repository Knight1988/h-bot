const QueryQueue = require('./QueryQueue.js');
const Message = require('discord.js').Message;
const YoutubeSearch = require('youtube-search');
const logger = require('../logger.js');

class YoutubeSearchQueue extends QueryQueue {
    /**
     *
     * @param query
     * @param {Message} requestMessage
     * @param {function} completeCallback
     */
    constructor(query, requestMessage, completeCallback) {
        super(query, requestMessage, completeCallback);

        let youtubeApiKey = process.env.YOUTUBE_API_KEY;
        if (!youtubeApiKey) {
            logger.error("Please set YOUTUBE_API_KEY as an environment variable");
            return;
        }
        let opts = {
            maxResults: 3,
            key: youtubeApiKey
        };
        YoutubeSearch(query, opts, function (err, result) {
            if (err) {
                logger.error(err);
                return;
            }

            let choiceMap = {};
            let promptText = "```Please choose one from below : \n";
            for (let i = 0; i < result.length; i++) {
                choiceMap[(i + 1) + ""] = result[i].link;
                promptText += ((i + 1) + ". " + result[i].title + "\n");
            }
            promptText += "```";
            this.showPrompt(promptText,choiceMap);
        }.bind(this));
    }
}

module.exports = YoutubeSearchQueue;