const Discord = require('discord.js');
const RichEmbed = Discord.RichEmbed;
const Channel = Discord.Channel;

class Util{
    static sendEmbeddedMessage(channel, title, messages, url) {
        if (!channel)
            return;

        let embed = new RichEmbed();

        messages = messages || [];
        if (messages.push !== Array.prototype.push)
            messages = [messages];

        if (title) {
            embed.title = title;
        }

        if (url)
            embed.url = url;

        for (let i = 0; i < messages.length; i += 2) {
            if (messages[i] && messages[i + 1]) {
                embed.addField(messages[i], messages[i + 1]);
            }
        }

        channel.send(embed);
    }

    static formatTimeLength(length){
        length = length || 0;
        let timeString = "";
        let hour = Math.floor(length / 3600);

        if (hour <= 0) {
        } else if (hour < 10)
            timeString += ("0" + hour + ":");
        else
            timeString += (hour + ":");

        let minute = Math.floor((length % 3600) / 60);
        if (minute < 10)
            timeString += ("0" + minute + ":");
        else
            timeString += (minute + ":");

        let second = length % 60;
        if (second < 10)
            timeString += ("0" + second);
        else
            timeString += second;

        return timeString;
    }

    static sendHTTPGet(url,cookies){
        return new Promise(function (resolve, reject) {
            const https = url.indexOf("https") !== -1 ? require('https') : require("http");

            url = url.substr(url.indexOf("://") + 3);
            if(url.indexOf('/') === -1)
                url += '/';
            let host = url.substr(0,url.indexOf('/'));
            let path = url.substr(url.indexOf('/'));

            let cookieString = "";
            cookies = cookies || [];
            let option ={
                host: host,
                path: path,
                headers: {
                    'Cookie' :cookies.join(";")
                }
            };
            https.get(option, (resp) => {
                let data = '';

                // A chunk of data has been recieved.
                resp.on('data', (chunk) => {
                    data += chunk;
                });

                // The whole response has been received. Print out the result.
                resp.on('end', () => {
                    resolve(data);
                });

            }).on("error", reject);
        });
    }

    static getXMLCDATA(xml,key){
        xml = xml.substr(0,xml.indexOf(`</${key}>`)); // cut tails
        xml = xml.substr(xml.indexOf(`<${key}>`) + `<${key}>`.length);
        return xml.split('<![CDATA[')[1].split(']]>')[0];
    }
}

module.exports  = Util;