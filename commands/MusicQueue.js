"use strict";
const Discord = require('discord.js');
const Message = Discord.Message;
const RichEmbed = Discord.RichEmbed;
const VoiceConnection = Discord.VoiceConnection;
const VoiceChannel = Discord.VoiceChannel;
const logger = require('../logger');
const YoutubePlayable = require('./Playable/YoutubePlayable.js');
const NCTPlayable = require('./Playable/NCTPlayable.js');
const CSNPlayable = require('./Playable/CSNPlayable.js');
const YoutubeSearchQueue = require('./YoutubeSearchQueue.js');
const Util = require('./utils.js');

class MusicQueue {

    /**
     *
     * @param {VoiceChannel} channel
     */
    constructor(channel) {
        this.setConnection(null);
        this._playQueue = [];
        this._voiceChannel = channel;
        this._currentDispatch = null;
        this._currentPlayable = null;
        this._playing = false;
        this._waitCount = 5; // after 5 schedule without any request, terminate the connection
        this._schedule = setInterval(this._onSchedule.bind(this), 1000);
    }

    /**
     *
     * @param {VoiceConnection} connection
     */
    setConnection(connection) {
        this._connection = connection;
    }

    _onSchedule() {
        if (this._playing)
            return;

        let playableItem = this._playQueue.splice(0, 1)[0]; // pop first item
        if (!playableItem) {
            if (this._connection && --this._waitCount <= 0) {
                this._connection.disconnect();
                this._connection = null;
                let channel = this._currentPlayable && this._currentPlayable.getRequestMessage().channel;
                Util.sendEmbeddedMessage(channel, "No song in queue. H is boring. H out.");
            }
            return;
        }

        this._waitCount = 10;
        this._playItem(playableItem);
    }

    /**
     *
     * @param {Playable} playableItem
     * @private
     */
    _playItem(playableItem) {
        if (this._playing)
            return;

        if (!this._connection) {
            this._voiceChannel.join()
                .then(function (connection) {
                    this._connection = connection;
                    this._playItem(playableItem);
                }.bind(this))
                .catch(logger.error);
            return;
        }

        let url = playableItem.getStreamUrl();
        let requestMessage = playableItem.getRequestMessage() || null;
        let channel = requestMessage && requestMessage.channel;
        let timeLength = Util.formatTimeLength(playableItem.getLength());

        // No stream url found
        if (!url) {
            Util.sendEmbeddedMessage(channel, "Error", [
                "Can't play",
                `[${playableItem.getTitle()}](${playableItem.getUrl()})`
            ]);
            logger.error("No stream url for media : " + playableItem.getUrl());
            return;
        }

        Util.sendEmbeddedMessage(channel, ":notes: Now playing : " + playableItem.getTitle(), [
            "Length",
            timeLength
        ], playableItem.getUrl());

        this._playing = true;
        this._currentDispatch = this._connection.playStream(url);
        this._currentPlayable = playableItem;

        this._currentDispatch.on('end', function () {
            this._currentDispatch = null;
            this._playing = false;
        }.bind(this));
    }

    static _getPlayable(urlOrQuery, requestMessage) {
        if (!(urlOrQuery && urlOrQuery.length))
            return;
        let playable = null;
        // parse schema
        let host = urlOrQuery.split("//")[1];
        if (host && host.substr(0, 15).indexOf("youtube.com") !== -1) {
            playable = new YoutubePlayable(urlOrQuery, requestMessage);
            return playable.fetch();
        } else if (host && host.substr(0, 26).indexOf("nhaccuatui.com") !== -1){
            playable = new NCTPlayable(urlOrQuery,requestMessage);
            return playable.fetch();
        }   else if (host && host.substr(0, 23).indexOf("chiasenhac.vn") !== -1){
            playable = new CSNPlayable(urlOrQuery,requestMessage);
            return playable.fetch();
        } else if (!host) {
            return new Promise(function (resolve, reject) {
                new YoutubeSearchQueue(urlOrQuery, requestMessage, function (url) {
                    if (!url)
                        return;
                    playable = new YoutubePlayable(url, requestMessage);
                    playable.fetch()
                        .then(resolve)
                        .catch(reject);
                });
            });
        }
        return Promise.reject("Unknown host : " + urlOrQuery);
    }


    /**
     *
     * @returns {Playable|*}
     */
    getCurrentPlayable() {
        return this._currentPlayable;
    }

    /**
     *
     * @param {string} url
     * @param {Message} requestMessage
     */
    enqueue(url, requestMessage) {
        MusicQueue._getPlayable(url, requestMessage)
            .then(function (playable) {

                // in case search don't return a result
                if (!playable)
                    return;

                this._playQueue.push(playable);
                Util.sendEmbeddedMessage(requestMessage.channel, "Enqueued " + playable.getTitle()
                    + " at position " + this._playQueue.length + "", [], playable.getUrl());
            }.bind(this))
            .catch(function (err) {
                logger.error(err);
                Util.sendEmbeddedMessage(requestMessage.channel, "Error", [
                    "Can't resolve url",
                    `${url}`
                ]);
            }.bind(this));
    }

    skip() {
        if (this._currentDispatch) {
            this._currentDispatch.end();
            this._currentDispatch = null;
            return true;
        } else
            return false;
    }

    stop() {
        this._playQueue = [];
        return this.skip();
    }

    /**
     *
     * @returns {boolean}
     */
    hasConnect() {
        return !!this._connection;
    }
}

module.exports = MusicQueue;