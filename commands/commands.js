const logger = require('../logger');
const Discord = require('discord.js');
const Message = Discord.Message;
const RichEmbed = Discord.RichEmbed;
const MusicQueue = require('./MusicQueue');
const SharedEvent = require('./SharedEvent.js');
const CPPCompiler = require('./CPPCompiler.js');
const TTS = require("./TTS.js");
const https = require('https');

/**
 *
 * @param {Message} message
 * @param {Array} args
 */
function say(message, args) {
    const sendContent = args.join(' ');
    if (sendContent.indexOf("dm") !== -1 || sendContent.indexOf("dit me") !== -1)
        message.channel.send("dm hoang");
    else
        message.channel.send(args.join(' '));
}

/**
 *
 * @param {Message} message
 * @param {Array} args
 */
function help(message, args) {
    message.channel.send("Hi. I'm H bot, the (H)ơn người bot.\n"
        + "Available commmands : \n"
        + " - help : Show this message.\n"
        + " - say : Say something.\n"
        + " - roll : Roll a random number between 0 - 1000\n"
        + " - rate : Rate someone or something\n"
        + " - play : Play music\n"
        + " - skip : Skip current song\n"
        + " - stop : Stop music\n");
    // message.channel.send("Help is for the weak!!!");
}

/**
 *
 * @param {Message} message
 * @param {Array} args
 */
function roll(message, args) {
    message.channel.send(Math.floor(Math.random() * 1000));
}

/**
 *
 * @param {Message} message
 * @param {Array} args
 */
function rate(message, args) {
    const specialRate = [
        "9/11",
        "420/69",
        "322/solo",
        "30/4/1975"
    ];

    let rate = Math.floor(Math.random() * (10 + specialRate.length) + 1);
    if (rate <= 10)
        rate += "/10";
    else
        rate = specialRate[rate - 11];

    const mentions = message.mentions.users.first() || args.join(' ') || "it";
    message.channel.send("I rate " + mentions + " " + rate);
}

function taixiu(message, args) {
    if (args.length == 0)
        args = null;
    args = args || ["heo", "khỉ", "chó", "nhện", "heo", "khỉ", "chó", "nhện", "Hoàng", "Mino"];
    message.channel.send(args[Math.floor(Math.random() * args.length)]);
}

let _voiceQueues = [];

/**
 *
 * @param {Message} message
 * @param {Array} args
 */
function play(message, args) {
    if (!message.guild)
        return;
    if (!args.length) {
        message.channel.send("Play wut ?");
        return;
    }

    const channel = message.member.voiceChannel;
    if (!channel) {
        message.channel.send(message.member + " is not connected to any voice channel.");
        return;
    }

    let queue = _voiceQueues[channel];
    let url = args[0].split("://") > 1 ? args[0] : args.join(' ');
    if (queue && queue.hasConnect()) {
        queue.enqueue(url, message);
        return;
    }
    queue = new MusicQueue(channel);
    _voiceQueues[channel] = queue;
    queue.enqueue(url, message);
}

/**
 *
 * @param {Message} message
 * @param args
 */
function skip(message, args) {
    if (!message.guild)
        return;

    const voiceChannel = message.member.voiceChannel;
    if (!voiceChannel) {
        message.channel.send(message.member + " is not connected to any voice channel.");
        return;
    }

    let queue = _voiceQueues[voiceChannel];
    let channel = message.channel;
    if (!(queue && queue.getCurrentPlayable())) {
        channel && channel.send("```No song to skip```");
        return;
    }
    let songTitle = queue.getCurrentPlayable().getTitle();
    if (queue && queue.skip()) {
        channel && channel.send("Skipped song **" + songTitle
            + "**");
    } else {
        channel && channel.send("```No song to skip```");
    }
}

function stop(message, args) {
    if (!message.guild)
        return;

    const voiceChannel = message.member.voiceChannel;
    if (!voiceChannel) {
        message.channel.send(message.member + " is not connected to any voice channel.");
        return;
    }

    let queue = _voiceQueues[voiceChannel];
    let channel = message.channel;
    if (queue && queue.stop())
        channel && channel.send("```Play queue cleared```");
    else
        channel && channel.send("```No song to skip```");
}

/**
 *
 * @param {Message} message
 * @param {Array} args
 */
function unknownCommand(message, args) {
    message.channel.send("Unknown command : " + args[0] + ".\n"
        + "Use h!help to show the help.");
}


let antiTable = {};

function makeAntiMessage(author, index) {
    let antiMessage = [
        "**Đm con " + author + " cút. Việc của mày à ?**",
        "**Bố giết mày giờ con " + author + " này**",
        "**Tao trẻ con với mày đấy hả " + author + "?**"
    ];

    index = index || 0;
    return antiMessage[index];
}

/**
 *
 * @param {Message} message
 */
function antiBot(message) {
    if (!message)
        return;

    if (message.author.bot) {
        let botName = message.author.username;
        let antiEntry = antiTable[message.author.username] || 0;
        message.channel.send(makeAntiMessage(message.member, antiEntry));
        antiEntry = (antiEntry + 1) % 3;
        antiTable[message.author.username] = antiEntry;
    }
}

/**
 *
 * @param {Message} message
 * @param args
 */
function ctest(message, args) {
    if (args.length < 1) {
        message.channel.send("Not enough argument(s)");
        return;
    }

    let source = args;

    //cut head
    let index = 0;
    let openBlockSyntaxes = ["```cpp", "```c", "```"];
    for (let i = 0; i < openBlockSyntaxes.length; i++) {
        if ((index = source.indexOf(openBlockSyntaxes[i])) !== -1) {
            source = source.substr(index + openBlockSyntaxes[i].length);
            break;
        }
    }

    //cut tail
    if ((index = source.indexOf("```")) !== -1)
        source = source.substr(0, index);

    CPPCompiler.compileAndRun(source)
        .then(function (output) {
            message.channel.send("**Output** : " + output);
        })
        .catch(function (err) {
            message.channel.send("**Error** : ```c" + err + "```");
        });
}

/**
 *
 * @param {Message} message
 * @param {boolean} generated : indicate if message is generated by code or not
 */
function processCommand(message, generated) {
    if (message.client.user.id === message.author.id)
        return;

    process.env.ANTI_ENABLED == "true" && antiBot(message);

    if (message.content.substr(0, 2).toLowerCase() === 'h!') {
        let args = message.content.substring(2).split(/[\s]+/);
        if (!args.length) {
            message.channel.send("Incorrect form of command");
            return;
        }
        let cmd = args.splice(0, 1)[0].toLowerCase();

        if (!generated)
            logger.info("User " + message.author.username + " requested command "
                + cmd + " with args : " + args.join(' '));

        switch (cmd) {
            case "say":
                say(message, args);
                break;
            case "help":
                help(message, args);
                break;
            case "rate":
                rate(message, args);
                break;
            case "roll":
                roll(message, args);
                break;
            case "play":
                play(message, args);
                break;
            case "skip":
                skip(message, args);
                break;
            case "stop":
                stop(message, args);
                break;
            case "en":
                TTS(message, args, "en-US");
                break;
            case "vi":
                TTS(message, args, "vi-VN");
                break;
            case "taixiu":
                taixiu(message, args);
                break;
            // case "ctest":
            //     ctest(message, message.content.substring(7));
            //     break;
            default:
                unknownCommand(message, [cmd].concat(args));
        }
    } else {
        SharedEvent.emit('discord_message', message);
    }
}

module.exports = processCommand;
