const Message = require('discord.js').Message;
const SharedEvent = require('./SharedEvent.js');
const logger = require('../logger.js');

const REQUEST_TIME_OUT_THRESHOLD = 15000; // 15s

class QueryQueue {
    /**
     *
     * @param query
     * @param {Message} requestMessage
     * @param {function} completeCallback
     */
    constructor(query, requestMessage, completeCallback) {
        this._query = query;
        this._channel = requestMessage.channel;
        this._owner = requestMessage.author;
        this._completeCallback = completeCallback;
        this._promptMessage = null;
        this._restrictToOwner = true;
        this._messageListener = this.onMessage.bind(this);
        this._timeout = setTimeout(this.onTimeout.bind(this), REQUEST_TIME_OUT_THRESHOLD);
        SharedEvent.addListener('discord_message', this._messageListener);
    }

    onTimeout() {
        this.setResult(null);
        this._channel && this._channel.send("Lâu la vl, bố nghỉ.");
    }

    /**
     *
     * @param {Message} message
     */
    onMessage(message) {
        if (!this._promptChoices)
            return;

        if(this._restrictToOwner && message.author !== this._owner){
            this._channel && this._channel.send("Bố đéo hỏi mày !!!");
            return;
        }

        let choice = this._promptChoices[message.content];
        if (!choice) {
            logger.info("User " + message.author.username + " provided an invalid choice : "
                + choice);
            this._channel && this._channel.send("Invalid answer!!");
            return;
        }

        this.setResult(choice);
        this._promptChoices = null;
    }

    /**
     *
     * @param {string} prompt
     * @param {object} choices
     */
    showPrompt(prompt, choices) {
        if (!this._channel)
            return;

        this._channel.send(prompt)
            .then(function (message) {
                this._promptMessage = message;
            }.bind(this));
        this._promptChoices = choices;
    }

    /**
     *
     * @param {boolean} isRestrict : indicate if only the owner can answer or not
     */
    setRestrictToOwner(isRestrict){
        this._restrictToOwner = isRestrict;
    }

    setResult(result) {
        if (this._completeCallback)
            this._completeCallback(result);
        this._completeCallback = null;
        SharedEvent.removeListener('discord_message', this._messageListener);
        this._messageListener = null;
        if (this._timeout)
            clearTimeout(this._timeout);
        if (this._promptMessage){
            this._promptMessage.delete();
            this._promptMessage = null;
        }
    }
}

module.exports = QueryQueue;