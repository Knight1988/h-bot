const {exec} = require('child_process');
const fs = require('fs');

function _generateFileName() {
    let randomChar = '1234567890qwertyuiopasdfghjklzxcvbnm';
    let result = "";
    for (let i = 0; i < 16; i++) {
        result += randomChar[Math.floor(Math.random() * randomChar.length)];
    }
    return result;
}

class CPPCompiler {
    static compileAndRun(source) {
        return new Promise(function (res, rej) {
            if (!fs.existsSync("./ctest"))
                fs.mkdirSync("./ctest");

            let filePath = null;
            let randomString = null;
            let executePath = null;
            do {
                randomString = _generateFileName();
                executePath = "./ctest/" + randomString;
                filePath = executePath + ".cpp";
            }
            while (fs.existsSync(filePath));

            fs.writeFileSync(filePath, source);
            exec(`g++ ${filePath} -o ${executePath}; ${executePath}; rm -f ${executePath}; rm -f ${filePath}`
                , null, function (err, stdout, stderr) {
                    if (stderr && stderr.length)
                        rej(stderr);
                    else
                        res(stdout);
                });
        });
    }
}

module.exports = CPPCompiler;