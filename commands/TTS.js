var https = require('https');
const logger = require('../logger');

function _checkArgsAndVoiceChannel(message, args) {
    if (args.length < 1) {
        message.channel.send("Not enough argument(s)");
        return false;
    }

    const channel = message.member.voiceChannel;
    if (!channel) {
        message.channel.send(message.member + " is not connected to any voice channel.");
        return false;
    }

    return true;
}

function getTTSLocation(id, callback) {
    var reqOption = {
        host: 'api.soundoftext.com',
        port: '443',
        path: '/sounds/' + id,
        method: 'GET'
    };

    var req = https.request(reqOption, function (res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            callback = callback || console.log;
            chunk = JSON.parse(chunk);
            if (chunk["status"] == "Done")
                callback(chunk["location"]);
            else if (chunk["status"] == "Pending")
                getTTSLocation(id, callback);
            else
                callback(false, chunk);
        });
    });

    req.end();
}

var queue = queue || [];
var playing = false;

function tts(message, args, voice) {
    if (!_checkArgsAndVoiceChannel(message, args))
        return;

    if (playing){
        queue.push({
            message:  message,
            args : args,
            voice : voice
        });
        return;
    }

    playing = true;

    var postData = {
        engine: "Google",
        data: {
            text: args.join(' '),
            voice: voice
        }
    };

    var post_options = {
        host: 'api.soundoftext.com',
        port: '443',
        path: '/sounds',
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        }
    };
    var postReq = https.request(post_options, function (res) {
        res.setEncoding('utf8');
        res.on('data', function (chunk) {
            chunk = JSON.parse(chunk);
            if (!chunk["success"])
                return;
            var id = chunk["id"];
            getTTSLocation(id, function (location, data) {
                if (!location)
                    logger.error(JSON.stringify(data));
                const channel = message.member.voiceChannel;
                var connection = channel.join()
                    .then(function (connection) {
                        var dispatch = connection.playStream(location);
                        dispatch.on('end', function () {
                            playing = false;
                            if (queue && queue.length){
                                var nextRequest = queue.splice(0,1)[0];
                                tts(nextRequest.message,nextRequest.args,nextRequest.voice);
                            }else{
                                connection.disconnect();
                            }
                        });
                    }.bind(this))
                    .catch(logger.error);
            });
        });
    });
    postReq.write(JSON.stringify(postData));
    postReq.end();
}

module.exports = tts;