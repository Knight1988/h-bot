const Playable = require('./Playable.js');
const Util = require('../utils.js');

function decode_download_url(var1, var2, var3, var4) {
    let var5 = ["U", "W", "J", "H", "D", "G", "M", "A", "Y", "I", "X", "N", "R", "L", "B", "P", "K"];
    let var6 = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "c", "u", "f", "r", "1", "1", "2"];

    if (var4 > 0) {
        for (let i = 0; i < var5.length; i++) {
            let re = new RegExp(var5[i], "g");
            var2 = var2.replace(re, var6[i]);
        }
    }

    return var1 + var2 + var3;
}

class CSNPlayable extends Playable {
    fetch() {
        let url = this._url;
        let host = "http://chiasenhac.vn/";
        if (!url)
            return Promise.reject("Url not specified");

        if (this._streamUrl)
            return Promise.resolve(this._streamUrl);

        return new Promise(function (res, rej) {
            Util.sendHTTPGet(url)
                .then(function (source) {
                    if (!source) {
                        rej("Can't fetch source code of : " + url);
                        return;
                    }
                    // // find the best quality page url
                    let path = source.substr(source.lastIndexOf("quality.php?q="));
                    let startIndex = "quality.php?q=".length;
                    let endIndex = path.indexOf('&');
                    let mq = `i%3A${path.substr(startIndex, endIndex - startIndex)}%3B`;
                    return Util.sendHTTPGet(url, [`mq=${mq}`]);
                }.bind(this))
                .catch(rej)

                // get the audio url
                .then(function (source) {
                    let sourceLines = source.split("\n");
                    let stringEval = null;
                    for (let i = 0; i < sourceLines.length; i++) {
                        if (sourceLines[i].indexOf("<meta name=\"title\"") !== -1) {
                            let startIndex = sourceLines[i].indexOf("content=\"") + "content=\"".length;
                            let trimLength = sourceLines[i].substr(startIndex).indexOf("\"");
                            let meta = sourceLines[i].substr(startIndex, trimLength);

                            meta = meta.split("~");
                            if (meta.length >= 2) {
                                this._title = meta[0].trim();
                                this._artist = meta[1].trim();
                            } else if (meta.length > 0) {
                                this._title = meta[0].trim();
                            }
                            continue;
                        }
                        if (sourceLines[i].indexOf("decode_download_url") !== -1) {
                            stringEval = sourceLines[i].substr(
                                sourceLines[i].indexOf("decode_download_url"));
                            break;
                        }
                    }

                    if (!stringEval)
                        rej("Can't find the audio url");
                    else {
                        this._streamUrl = eval(stringEval);
                        res(this);
                    }
                }.bind(this))
                .catch(rej);
        }.bind(this));
    }

    getStreamUrl() {
        return this._streamUrl;
    }
}

module.exports = CSNPlayable;