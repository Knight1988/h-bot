const Message = require('discord.js').Message;

class Playable {
    /**
     *
     * @param {string} url
     * @param {Message} requestMessage
     */
    constructor(url, requestMessage) {
        this._title = "";
        this._artist = "";
        this._length = "";
        this._url = url;
        this._requestMessage = requestMessage;
    }

    getTitle() {
        if (this._artist && this._artist.length)
            return this._title + " - " + this._artist;
        return this._title;
    }

    getArtist() {
        return this._artist;
    }

    getLength() {
        return this._length;
    }

    getUrl() {
        return this._url;
    }

    /**
     *
     * @returns {Message|*}
     */
    getRequestMessage(){
        return this._requestMessage;
    }

    fetch() {
        return Promise.reject("Not implemented method : fetch");
    }

    getStreamUrl() {
        return null;
    }
}

module.exports = Playable;