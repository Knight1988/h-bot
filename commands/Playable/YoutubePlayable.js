const ytdl = require('ytdl-core');
const Playable = require('./Playable.js');

class YoutubePlayable extends Playable {
    fetch() {
        let url = this._url;
        if (!url)
            return Promise.reject("Url not specified");

        if (this._streamUrl)
            return Promise.resolve(this._streamUrl);

        return new Promise(function (res, rej) {
            let option = {
                filter: format => {
                    return format.container === 'mp4' && format.encoding === null
                }
            };

            ytdl.getInfo(url, option)
                .then(function (info) {
                    this._loadInfo(info);
                    res(this);
                }.bind(this))
                .catch(rej);
        }.bind(this));
    }

    _loadInfo(info){
        let title = info.title.split(' - ', 2);

        if(title.length >= 2) {
            this._artist = title[0];
            this._title = title[1];
        } else {
            this._title = info.title;
        }

        this._length = info.length_seconds;

        // stream url
        let highestAudioBitRate = 0;
        for (let i = 0;i<info.formats.length;i++){
            let format = info.formats[i];
            if (format.encoding === null){
                if (format.audioBitrate <= highestAudioBitRate)
                    break;
                this._streamUrl = format.url;
                highestAudioBitRate = format.audioBitrate;
            }
        }
    }

    getStreamUrl(){
        return this._streamUrl;
    }
}

module.exports = YoutubePlayable;