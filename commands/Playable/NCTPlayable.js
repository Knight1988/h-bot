const Playable = require('./Playable.js');
const Util = require('../utils.js');

class NCTPlayable extends Playable {
    fetch() {
        let url = this._url;
        if (!url)
            return Promise.reject("Url not specified");

        if (this._streamUrl)
            return Promise.resolve(this._streamUrl);

        return new Promise(function (res, rej) {
            Util.sendHTTPGet(url)
                .then(function (source) {
                    return this._loadInfo(source);
                }.bind(this))
                .catch(rej)

                .then(function () {
                    res(this);
                }.bind(this))
                .catch(rej);
        }.bind(this));
    }

    _loadInfo(source) {
        let lines = source.split('\n');
        let xmlUrl = null;
        for (let i = 0; i < lines.length; i++) {
            if (lines[i].indexOf("player.peConfig.xmlURL") === -1)
                continue;

            xmlUrl = lines[i].split('"')[1];
            break;
        }

        if (!xmlUrl)
            return Promise.reject("Can't resolve " + this.getUrl());

        return new Promise(function (res,rej) {
            Util.sendHTTPGet(xmlUrl)
                .then(function (xml) {
                    this._streamUrl = Util.getXMLCDATA(xml,"location");
                    this._title = Util.getXMLCDATA(xml,"title");
                    this._artist = Util.getXMLCDATA(xml,"creator");
                    res();
                }.bind(this))
                .catch(rej);
        }.bind(this));
    }

    getStreamUrl(){
        return this._streamUrl;
    }
}

module.exports = NCTPlayable;