function getTime(){
    var date = new Date();

    var d = date.getDate();
    if (d < 10) d = "0" + d;

    var m = date.getMonth();
    if (m < 10) m = "0" + m;

    var y = date.getFullYear();

    var h = date.getHours();
    if (h < 10) h = "0" + h;

    var M = date.getMinutes();
    if (M < 10) M = "0" + M;

    var s = date.getSeconds();
    if (s < 10) s = "0" + s;

    return d + "/" + m + "/" + y + " " + h + ":" + M + ":" + s;
}

function info(str){
    console.log(getTime() + " Info : " + str);
}

function error(str){
    console.trace();
    console.log(getTime() + " Error : " + str);
}

function debug(str){
    console.log(getTime() + " Debug : " + str);
}

module.exports = {
    info : info,
    error : error,
    debug : debug
};